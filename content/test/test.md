---
title: Test
subtitle: Why you'd want to hang out with me
comments: false
---


# Operation SummerSault


This is the core of the operation

## Checklists

Did you find all clues
* [X] First 
* [ ] Second

## Tables

| d4 | Description |
| :------: | ----------- |
| 1        | Deep One. |
| 2        | Thoughtful One. |
| 3        | Ascetic One. |
| 4        | Avatar. |

## Graphs and Diagrams
<div class="mermaid">
flowchart TB
    subgraph debrefing
    a1[Enter the scene of the crime ]-->a2[talk with the handler]
    end
    subgraph id1[the apartment]
    b1{did you avoid detection}--YES-->b2[deal with noisy neighbor]
    b1--NO-->b3[you are in]
    b3-->b4[find key of cabin]
    b2-->b4
    end
    subgraph id2[the cabin]
    c1[open door]-->c2[find locked basement]
    c1[open door]-.->c3[find hidden stash]
    c2-->c4{confront enemy}
    c3-->c4
    end
    debrefing --> id1[the apartment]
    id2 --> id1
    id1 --> id2
</div>
<script async src="https://unpkg.com/mermaid@8.2.3/dist/mermaid.min.js"></script>

```mermaid
flowchart TB
    subgraph debrefing
    a1[Enter the scene of the crime ]-->a2[talk with the handler]
    end
    subgraph id1[the apartment]
    b1{did you avoid detection}--YES-->b2[deal with noisy neighbor]
    b1--NO-->b3[you are in]
    b3-->b4[find key of cabin]
    b2-->b4
    end
    subgraph id2[the cabin]
    c1[open door]-->c2[find locked basement]
    c1[open door]-.->c3[find hidden stash]
    c2-->c4{confront enemy}
    c3-->c4
    end
    debrefing --> id1[the apartment]
    id2 --> id1
    id1 --> id2
```

## Music


```abc
T:Red Book Sonata, The   % title
T:King in Yellow         % an alternative title
M:4/4
DEFG ABcd|\
DEFG ABcd|\
DEFG ABcd|]
DEFG ABcd|\
DEFG ABcd|\
K:C Major
DEFG ABcd|]
T:when the king wants you at the ball
DEFG ABcd|\
DEFG ABcd|\
DEFG ABcd|]
T:you can't escape his invitation
DEFG ABcd|\
K:C Minor
DEFG ABcd|\
K:Cm
DEFG ABcd|]
T:Always wear a mask
DEFG ABcd|\
K:C Phrygian
DEFG ABcd|\
K:C Locrian
DEFG ABcd|]
```
